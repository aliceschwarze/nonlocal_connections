# This script contains the functions that we use in the "main" IPython notebook.

################################################################################

#import libraries
import matplotlib
import numpy as np
from scipy.special import binom
from matplotlib import pyplot as plt
import matplotlib.animation as anim
from IPython.display import HTML

################################################################################

def lattice_adjacency_matrix(L, d, norm=True):
    '''Create adjacency matrix for a (hyper-)cube in d dimensions with edge 
    length L.'''
    
    if not isinstance(L, (int, float)):
        print('Error in lattice_adjacency_matrix: L must be an integer or float.')
        return 0
    
    if not isinstance(d, (int, float)):
        print('Error in lattice_adjacency_matrix: d must be an integer or float.')
        return 0

    #create empty adjacency matrix
    num_vertices = L**d
    adj_matrix = np.zeros((int(num_vertices),int(num_vertices)))
    
    #add edges
    # compare https://brilliant.org/discussions/thread/adjacency-matrix-for-a-square-lattice/
    for v in range(num_vertices):
        for i in range(d):
             if int(np.floor(v/L**(i+1))) == int(np.floor((v+L**i)/L**(i+1))): # <= num_vertices-1:
                adj_matrix[v, int(v+L**i)] = 1.0    
            
    #symmetrize
    adj_matrix += adj_matrix.T
    
    #normalize to Frobenius norm=1
    if norm:
        # use Frobenius norm
        #adj_matrix = adj_matrix/float(np.sum(adj_matrix))
        # use spectral norm
        adj_matrix = adj_matrix/np.max(np.abs(np.linalg.eigvals(adj_matrix)))
    
    return adj_matrix

################################################################################

def run_simulation(K, x0=None, s=1.0, t=1, dt=0.05):
    '''Run simulation of noisy lineary dynamics on a network with deterministic
    transfer matrix `K`, initial state `x0` (1d array), noise intensity `s`.'''
    
    # define "push-forward" operator via lambda function (compare SDE)
    push = lambda x: (x + np.matmul(K,x)*dt 
                      + s*np.random.normal(size=x.shape)*dt)
    
    # set initial condition
    if x0 is None: 
        # if x0 has default value, replace by 1d array with correct dimensions
        x0 = np.zeros(len(K))
        
    # set number of time steps
    steps = int(np.ceil(t/dt))
        
    # run simulation
    sim = np.empty((steps+1,x0.size)) #initialize array for data
    X = x0 #at the beginning the state X is given by initial state x0
    for i in range(steps):
        sim[i]= X #store current state in data array
        X = push(X) #compute next state
    sim[i]= X #store final state in data array
    
    #return data array
    return sim 

################################################################################

def transfer_spreading(l, d, theta, epsilon, normalized_adjacency=True):
    '''Generate the transfer matrix for a deterministic linear spreading process 
    with time-scale parameter `theta` and coupling parameter `epsilon` on a
    `d`-dimensional lattice of `l`**d nodes. The transfer matrix depends only on 
    the product epsilon and not on theta.'''
    
    # normalized adjacency matrix
    A = lattice_adjacency_matrix(l, d, norm=normalized_adjacency)
    
    return epsilon*A

    
def transfer_diffusion(l, d, theta, epsilon, normalized_adjacency=True):
    '''Generate the transfer matrix for a deterministic linear diffusion process 
    with time-scale parameter `theta` and coupling parameter `epsilon` on a
    `d`-dimensional lattice of `l`**d nodes. The transfer matrix depends only on 
    the epsilon and not on theta.'''

    # normalized adjacency matrix
    A = lattice_adjacency_matrix(l, d, norm=normalized_adjacency)
    # degree matrix
    D = np.diag(np.sum(A, axis=1))
    # (negative of) Laplace matrix
    L_minus = epsilon*(A-D)
    
    return L_minus


def transfer_OU(l, d, theta, epsilon, normalized_adjacency=True):
    '''Generate the transfer matrix for a determinstic part of Ornstein-
    Uhlenbeck process with time-scale parameter `theta` and coupling parameter 
    `epsilon` on a `d`-dimensional lattice of `l`**d nodes. For this diffusion 
    process, the transfer matrix depends only on the product theta*epsilon and 
    not theta and epsilon separately.'''

    # normalized adjacency matrix
    A = lattice_adjacency_matrix(l, d, norm=normalized_adjacency)
    # transfer matrix (without time-scale parameter)
    K = epsilon*A-np.eye(len(A))
                         
    return theta*K
                         
################################################################################

def demo(K, x0, s=1.0, title='', t=1, dt=0.05, sampling_rate=20): #TODO
    # need matplotlib backend agg
    
    #make data                     
    data = run_simulation(K, x0=x0, s=s, t=t, dt=dt)
                         
    # make empty figure                     
    fig, ax = plt.subplots()
    fig.title = title
                         
    # set axis limits                     
    l = int(np.sqrt(len(x0))) #length of square grid in each dimension
    ax.axis([0, l-1, 0, l-1])

    # make empty heat map 
    mean, std = np.mean(data), np.std(data)
    im = ax.imshow(data[0].reshape((l,l)), cmap='Blues', 
                   vmin=max([0, mean-2*std]),vmax=mean+2*std)
                         
    # make colorbar
    #ax.colorbar(vmin=np.min(data),vmax=np.max(data))
    fig.colorbar(im)#, cax=cax, orientation='horizontal')
    
    # make animation function
    def animate(i):
        im.set_data(data[sampling_rate*i].reshape((l,l)))

    # create animation                     
    ani = anim.FuncAnimation(fig, animate, frames=int(len(data)/sampling_rate))
                         
    return ani
 
                         
def demo_spreading(l=5, theta=1.0, epsilon=0.5, s=1.0, x0=None, t=1, dt=0.05, 
                   sampling_rate=20, normalized_adjacency=False): 
    '''Make a demo of spreading process with time-scale parameter `theta`,
    coupling parameter `epsilon`, noise strength `s` and initial condition `x0`
    on a square lattice of `l**2` nodes. The initial condition `x0` must be a 
    1d array.'''
                         
    # make transfer matrix                     
    K = transfer_spreading(l, 2, theta, epsilon, normalized_adjacency=normalized_adjacency)
    
    # if non given, set initial condition to delta function
    if x0 is None:
        x0 = np.zeros(l**2)
        x0[int(np.floor(l/2)+np.floor(l/2)*l)] = l**2
    
    return demo(K, x0, s=s, title='Spreading process', t=t, dt=dt, 
                sampling_rate=sampling_rate)
               
                         
def demo_diffusion(l=5, theta=1.0, epsilon=0.5, s=1.0, x0=None, t=1, dt=0.05, 
                   sampling_rate=20, normalized_adjacency=False): 
    '''Make a demo of diffusion process with time-scale parameter `theta`,
    coupling parameter `epsilon`, noise strength `s` and initial condition `x0`
    on a square lattice of `l**2` nodes. The initial condition `x0` must be a 
    1d array.'''
                         
    # make transfer matrix 
    K = transfer_diffusion(l, 2, theta, epsilon, normalized_adjacency=normalized_adjacency)
                         
    # if non given, set initial condition to delta function
    if x0 is None:
        x0 = np.zeros(l**2)
        x0[int(np.floor(l/2)+np.floor(l/2)*l)] = l**2
                         
    return demo(K, x0, s=s, title='Diffusion process', t=t, dt=dt, sampling_rate=sampling_rate)
                    
                         
def demo_Ornstein(l=5, theta=1.0, epsilon=0.5, s=1.0, x0=None, t=1, dt=0.05, 
                  sampling_rate=20, normalized_adjacency=False): 
    '''Make a demo of an Ornstein-Uhlenbeck process with time-scale parameter 
    `theta`, coupling parameter `epsilon`, noise strength `s` and initial 
    condition `x0` on a square lattice of `l**2` nodes. The initial condition 
    `x0` must be a 1d array.'''

    # make transfer matrix 
    K = transfer_OU(l, 2, theta, epsilon, normalized_adjacency=normalized_adjacency)  
                         
    # if non given, set initial condition to delta function
    if x0 is None:
        x0 = np.zeros(l**2)
        x0[int(np.floor(l/2)+np.floor(l/2)*l)] = l**2
                         
    return demo(K, x0, s=s, title='OU process', t=t, dt=dt, sampling_rate=sampling_rate)
                
                         
def cov_to_corr(cov):
    '''Compute correlation matrix from covariance matrix.'''
    
    # get variances from diagonal elements of covariance matrix
    variances = np.diag(cov)
    # create a 2d-array with entries x_{ij} = cov_{ii}*cov_{jj}
    divisors = np.sqrt(np.kron(variances, variances).reshape((len(cov),len(cov))) )
    # correlations are cov_{ij}/(cov_{ii}*cov_{jj})
    corr = cov/divisors
                       
    return corr
    
                         
def cov_approx(K,max_m):
    '''Compute approximate covariance matrix for process with transfer matrix K
    to order `max_m` from sum  expression of the solution of the Lyapunov 
    equation.'''
                         
    # shift transfer matrix by identity
    K1 = K + np.eye(len(K))  
    # transpose of shifted transfer matrix                     
    K1T = K1.T

    # start with empty covariance matrix
    sigma = np.zeros(K.shape)
                         
    for m in range(max_m+1):
        # compute m-th order
        for k in range(m+1):
            # compute the k-th term of the m-th order
            d_sigma = np.matmul(np.linalg.matrix_power(K1,m-k),np.linalg.matrix_power(K1T,k))
            
            # include the weighting factor
            d_sigma = d_sigma #* binom(m,k)/0.5**(m+1)
        
        # add m-th order term to covariance matrix
        sigma += d_sigma
                         
    return sigma                
    
################################################################################

#deprecated functions

def run_dynamics(alpha, theta, epsilon, s, A, X0, steps=500):
    '''Run simulation of spread-diffusion dynamics on network with adjacency
    matrix A (2d array) and initial condition X0 (1D array). Parameter alpha 
    sets the "diffusiveness" of dynamics. (For alpha=1, simulate heat equation. 
    For alpha=0, simulate spreading process.) Parameter theta sets coupling 
    parameter (corresponds to diffusion constant when alpha=1 and to signal 
    decay rate when alpha=0). Parameter epsilon is the coupling parameter.
    Parameter s is the noise strength.
    
    Returns array (list of state vectors).'''
    
    # degree matrix
    D = np.diag(np.sum(A, axis=0)) #TODO: does this work?
    
    # Laplace matrix
    L = alpha*D-epsilon*A
    
    # define propagation operator that takes state X(t) to X(t+1)
    push = lambda x: x-theta*np.matmul(L,x)+ s*np.random.normal(size=x.shape)
    
    # run simulation
    sim = np.empty((steps+1,X0.size))
    X = X0
    for s in range(steps):
        sim[s]= X
        X = push(X)
    
    return sim